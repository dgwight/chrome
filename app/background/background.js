
chrome.browserAction.onClicked.addListener(function(activeTab) {
  chrome.storage.sync.get('otechieURL', function(value) {
    if (value && value.otechieURL) {
      chrome.tabs.create({ url: value.otechieURL });
    } else {
      chrome.tabs.create({ url: 'https://www.otechie.com' });
    }
  });
});

chrome.runtime.onMessageExternal.addListener((message, sender, sendResponse) => {
  switch (message && message.type) {
    case 'getUserScreen':
      if (sender && sender.url) {
        chrome.storage.sync.set({'otechieURL': sender.url});
      }
      handleGetUserScreenRequest(message.sources, sender.tab, sendResponse);
      break;

    case 'checkInstalled':
      sendResponse({
        type: 'success',
        message: 'Extension Installed'
      });
      break;

    default:
      sendResponse({
        type: 'error',
        message: 'Unrecognized request'
      });
      break;
  }

  return true;
});

function handleGetUserScreenRequest(sources, tab, sendResponse) {
  chrome.desktopCapture.chooseDesktopMedia(sources, tab, streamId => {
    if (!streamId) {
      sendResponse({
        type: 'error',
        message: 'Failed to get stream ID'
      });
    }

    sendResponse({
      type: 'success',
      streamId: streamId
    });
  });
}
